This project is deprecated.

See the updated project here: [https://gitlab.com/calincs/infrastructure/3m-docker](https://gitlab.com/calincs/infrastructure/3m-docker)

# X3ML 3M chart

This project is based on [https://github.com/ymark/3M-docker](https://github.com/ymark/3M-docker)

The Docker image `marketak/3m-docker` is located here: [https://hub.docker.com/r/marketak/3m-docker](https://hub.docker.com/r/marketak/3m-docker)

A `docker-compose.yml` file, a Kubernetes Helm chart, and more control over deployment parameters were added.
