#!/bin/bash

# force hostname lookup to loopback
if [ -z "$HOSTNAME" ]; then
  echo "HOSTNAME is not set. No changes made to /etc/hosts"
else
  echo "adding $HOSTNAME to /etc/hosts"
  echo "127.0.0.1 $HOSTNAME" >>/etc/hosts
fi

# redirect root requests
rm $CATALINA_HOME/webapps/ROOT/index.jsp
echo "<script language="javascript">location.href='/3M';</script>" >>$CATALINA_HOME/webapps/ROOT/index.jsp

# catch missing env vars
if [ -z "$SERVICE_ENDPOINT" ]; then
  echo "The parameter for the endpoint url is missing (SERVICE_ENDPOINT)"
  SERVICE_ENDPOINT="http://localhost"
  echo "Setting the SERVICE_ENDPOINT to $SERVICE_ENDPOINT"
fi
if [ -z "$EXIST_ENDPOINT" ]; then
  echo "The parameter for setting the exist database hostname is missing (EXIST_ENDPOINT)"
  EXIST_ENDPOINT="xmldb:exist://localhost:8081/exist"
  echo "Setting the EXIST_HOSTNAME to $EXIST_ENDPOINT"
fi

# inject hostnames from env vars
SERVICE_INTERNAL="http://localhost"
echo "injecting 3M url ${SERVICE_ENDPOINT}/3M and exist url ${EXIST_ENDPOINT}"
sed -i "s%http://255.255.255.255:8080%${SERVICE_INTERNAL}%g" $CATALINA_HOME/webapps/3M/WEB-INF/web.xml
sed -i "s%xmldb:exist://255.255.255.255:8081/exist%${EXIST_ENDPOINT}%g" $CATALINA_HOME/webapps/3M/WEB-INF/web.xml

sed -i "s%xmldb:exist://localhost:8081/exist%${EXIST_ENDPOINT}%g" $CATALINA_HOME/webapps/3MEditor/WEB-INF/web.xml
sed -i "s%xmldb:exist://localhost:8080/exist%${EXIST_ENDPOINT}%g" $CATALINA_HOME/webapps/3MEditor/WEB-INF/web.xml
sed -i "s%xmldb:exist://255.255.255.255:8081/exist%${EXIST_ENDPOINT}%g" $CATALINA_HOME/webapps/3MEditor/WEB-INF/web.xml
sed -i "s%xmldb:exist://255.255.255.255:8080/exist%${EXIST_ENDPOINT}%g" $CATALINA_HOME/webapps/3MEditor/WEB-INF/web.xml

sed -i "s%http://255.255.255.255:8080%${SERVICE_ENDPOINT}%g" $CATALINA_HOME/webapps/Maze/index.html
sed -i "s%http://255.255.255.255:8080%${SERVICE_ENDPOINT}%g" $CATALINA_HOME/webapps/Maze/errorpage.html
sed -i "s%http://255.255.255.255:8080%${SERVICE_ENDPOINT}%g" $CATALINA_HOME/webapps/Maze/multmappings.html
sed -i "s%http://255.255.255.255:8080%${SERVICE_ENDPOINT}%g" $CATALINA_HOME/webapps/Maze/singlemapping.html
sed -i "s%http://255.255.255.255:8080%${SERVICE_ENDPOINT}%g" $CATALINA_HOME/webapps/Maze/about.html
sed -i "s%http://255.255.255.255:8080%${SERVICE_INTERNAL}%g" $CATALINA_HOME/webapps/Maze/WEB-INF/config.properties
sed -i "s%http://255.255.255.255:8080%${SERVICE_INTERNAL}%g" $CATALINA_HOME/webapps/Maze/app/js/Controller.js
sed -i "s%http://'+window.location.hostname+':8080%${SERVICE_ENDPOINT}%g" $CATALINA_HOME/webapps/Maze/app/js/Controller.js

# change Apache port from 8080 to 80
sed -i "s%8080%80%g" $CATALINA_HOME/conf/server.xml
grep -rl ":8080" $CATALINA_HOME/webapps | xargs sed -i "s%:8080%:80%g"

# point persisting folders to a single volume mount
echo "copying 3M, eXist, and Apache data to mounted storage..."
mkdir -p /mnt/3M \
  && cp -npr /opt/3M/* /mnt/3M \
  && rm -rf /opt/3M \
  && ln -s /mnt/3M /opt/3M
mkdir -p /mnt/exist \
  && cp -npr /opt/exist/webapp/WEB-INF/data/* /mnt/exist \
  && rm -rf /opt/exist/webapp/WEB-INF/data \
  && ln -s /mnt/exist /opt/exist/webapp/WEB-INF/data
# mkdir -p /mnt/apache \
#   && cp -npr /opt/apache-tomcat-8.0.53/* /mnt/apache \
#   && rm -rf /opt/apache-tomcat-8.0.53 \
#   && ln -s /mnt/apache /opt/apache-tomcat-8.0.53
echo "files copied and symblic links created"

# start eXist-db and Apache Tomcat
exec supervisord -n
